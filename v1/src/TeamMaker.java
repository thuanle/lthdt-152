/**
 * <p>This class is used for creating the team in a combat.</p>
 * <p>
 * <p> <b>Note: This class will be replaced by other version when being judged.</b></p>
 */
public class TeamMaker {
    private static Combatable makeRandomTeam1Member() {
        return new DummyCombatable();
    }

    private static Combatable makeRandomTeam2Member() {
        return new DummyCombatable();
    }

    public static Combatable[] makeTeam1() {
        Combatable[] knights = new Combatable[3];
        for (int i = 0; i < knights.length; i++) {
            knights[i] = makeRandomTeam1Member();
        }
        return knights;
    }

    public static Combatable[] makeTeam2() {
        Combatable[] warriors = new Combatable[3];
        for (int i = 0; i < warriors.length; i++) {
            warriors[i] = makeRandomTeam2Member();
        }
        return warriors;
    }
}

class DummyCombatable implements Combatable {
    @Override
    public double getCombatScore() {
        return 0;
    }
}