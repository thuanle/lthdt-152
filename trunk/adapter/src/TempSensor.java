/**
 * Created by thuanle on 5/9/16.
 */
public interface TempSensor {
    double getTemp();
}
