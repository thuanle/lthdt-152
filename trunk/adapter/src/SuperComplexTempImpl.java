import java.util.Random;

/**
 * Created by thuanle on 5/9/16.
 */
public class SuperComplexTempImpl implements ISuperComplexTemp {
    private Random random = new Random();

    @Override
    public int getRunMode() {
        return randomInt(1, 2);
    }

    @Override
    public double getRawData(int mode) {
        switch (mode) {
            case 1:
                return randomInt(4, 45);
            case 2:
                return randomInt(270, 500);
            default:
                return -1000;
        }
    }

    @Override
    public double refineC(double c) {
        return c * randomDouble(0.95, 1.05);
    }

    @Override
    public double refineK(double k) {
        return k + 50 * randomDouble(-1, 1);
    }

    private double randomDouble(double from, double to) {
        double d = to - from;
        return random.nextDouble() * d + from;
    }

    private int randomInt(int from, int to) {
        int d = to - from;
        return random.nextInt(d + 1) + from;
    }
}
