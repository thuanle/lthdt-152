public class Main {

    public static void main(String[] args) {
        TempSensor[] sensors = SensorsMaker.createSensors();

        double totalTemp = 0;
        for (int i = 0; i < sensors.length; i++) {
            totalTemp += sensors[i].getTemp();
        }
        double avg = totalTemp / sensors.length;

        System.out.println("Average temp is " + avg);
    }
}
