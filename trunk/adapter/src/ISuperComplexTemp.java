/**
 * Created by thuanle on 5/9/16.
 */
public interface ISuperComplexTemp {
    double getRawData(int mode);

    int getRunMode();

    double refineC(double c);

    double refineK(double k);
}
