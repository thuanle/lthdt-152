import org.junit.Test;

/**
 * Created by thuanle on 5/11/16.
 */
public class ComplexTest {
    @Test
    public void testName() {
        UtilityTest.testClassName(TestConfig.COMPLEX);
    }

    @Test
    public void testConstructor() {
        UtilityTest.testConstructor(TestConfig.COMPLEX, double.class.getName(), double.class.getName());
    }
}
