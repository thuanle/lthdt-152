import java.util.Arrays;
import java.util.Random;

/**
 * Created by thuanle on 5/12/16.
 */
public class RandomHelper {
    public static int nextInt(int from, int end) {
        return new Random().nextInt(end - from + 1) + from;
    }

    public static int nextIntNotIn(int from, int end, int[] excludes) {
        int x;
        do {
            x = nextInt(from, end);
        } while (Arrays.binarySearch(excludes, x) >= 0);
        return x;
    }

    public static int nextIntIn(int[] include, int fromIndex) {
        return include[nextInt(fromIndex, include.length - 1)];
    }

    public static double nextDouble(int from, int to) {
        return new Random().nextDouble() * (to - from) + from;
    }
}
