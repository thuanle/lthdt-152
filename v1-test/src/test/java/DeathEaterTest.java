import org.junit.Test;

/**
 * Created by thuanle on 5/11/16.
 */
public class DeathEaterTest extends BaseCombatableTest {
    public DeathEaterTest() {
        super(ReflectionHelper.ClassDescriptionBuilder
                .name(TestConfig.DEATH_EATER)
                .baseClass(TestConfig.MONSTER)
                .addConstructor(TestConfig.COMPLEX)
                .addInterface(TestConfig.COMBATABLE)
                .addMethod(TestConfig.METHOD__COMBATABLE_GETCOMBATSCORE)
                .build());
    }


    @Test
    public void testCombatScore1() {
        double im = RandomHelper.nextDouble(1,400);
        double re = RandomHelper.nextDouble(1,400);
        double v = Math.sqrt(im*im + re*re);

        testMonsterCombatScore(GROUND_NORMAL_1, re, im, v);
    }


    @Test
    public void testCombatScore2() {
        double im = RandomHelper.nextDouble(1,400);
        double re = RandomHelper.nextDouble(1,400);
        double v = Math.sqrt(im*im + re*re);

        testMonsterCombatScore(GROUND_NORMAL_2, re, im, v);
    }

    @Test
    public void testCombatScore3() {
        double im = RandomHelper.nextDouble(1,400);
        double re = RandomHelper.nextDouble(1,400);
        double v = Math.sqrt(im*im + re*re);

        testMonsterCombatScore(GROUND_PRIME, re, im, v);
    }

    @Test
    public void testCombatScore4() {
        double im = RandomHelper.nextDouble(1,400);
        double re = RandomHelper.nextDouble(1,400);
        double v = Math.sqrt(im*im + re*re);

        testMonsterCombatScore(GROUND_SQUARE, re, im, v);
    }
}
