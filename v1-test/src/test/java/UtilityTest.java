import org.junit.Assert;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Created by thuanle on 5/11/16.
 */
public class UtilityTest {
    public static Class getClass(String name) throws ClassNotFoundException {
        switch (name) {
            case "boolean":
                return boolean.class;
            case "short":
                return short.class;
            case "int":
                return int.class;
            case "long":
                return long.class;
            case "char":
                return char.class;
            case "byte":
                return byte.class;
            case "float":
                return float.class;
            case "double":
                return double.class;
        }
        return Class.forName(name);
    }

    public static Class[] getClasses(String[] names) throws ClassNotFoundException {
        if (names == null) {
            return null;
        }

        Class[] spC = new Class[names.length];
        for (int i = 0; i < names.length; i++) {
            String symbolParam = names[i];
            spC[i] = UtilityTest.getClass(symbolParam);
        }
        return spC;
    }

    public static void testClassBase(String className, String baseName) {
        System.out.printf("Test Class %s, Base %s.\n", className, baseName);
        try {
            Class c = Class.forName(className);
            assertEquals(baseName, c.getSuperclass().getName());
        } catch (ClassNotFoundException e) {
            fail();
        }
    }

    public static void testClassInterface(String className, String interfaceName) {
        System.out.printf("Test Class %s, Interface %s.\n", className, interfaceName);
        try {
            Class c = Class.forName(className);
            Class[] ifs = c.getInterfaces();
            Assert.assertTrue(Arrays.stream(ifs)
                    .anyMatch(aClass -> aClass.getName().equals(interfaceName)));
        } catch (ClassNotFoundException e) {
            fail();
        }
    }

    /**
     * Test whether a class with given name
     *
     * @param name
     */
    public static void testClassName(String name) {
        System.out.printf("Test Class %s, Name %s.\n", name, name);
        try {
            Class.forName(name);
        } catch (ClassNotFoundException e) {
            fail();
        }
    }


    public static void testConstructor(String className, String... params) {
        System.out.printf("Test Class %s, Constructor %s.\n", className, Arrays.toString(params));
        try {
            Class c = Class.forName(className);
            c.getConstructor(UtilityTest.getClasses(params));
        } catch (ClassNotFoundException | NoSuchMethodException e) {
            fail();
        }
    }

    public static void testConstructorCount(String className, int count) {
        System.out.printf("Test Class %s, Constructor(s) %d.\n", className, count);
        try {
            Class c = Class.forName(className);
            Constructor[] cons = c.getConstructors();

            assertEquals(count, cons.length);
        } catch (ClassNotFoundException e) {
            fail();
        }
    }

    public static void testFieldCount(String className, int count) {
        System.out.printf("Test Class %s, Field(s) %d.\n", className, count);
        try {
            Class c = Class.forName(className);
            Field[] fs = c.getFields();

            assertEquals(count, fs.length);
        } catch (ClassNotFoundException e) {
            fail();
        }
    }

    public static void testInterfaceCount(String className, int count) {
        System.out.printf("Test Class %s, Interface(s) %d.\n", className, count);
        try {
            Class c = Class.forName(className);
            Class[] is = c.getInterfaces();
            assertEquals(count, is.length);
        } catch (ClassNotFoundException e) {
            fail();
        }
    }

    /**
     * Test a class with given method parameter
     *
     * @param className
     * @param methodName
     * @param params
     */
    public static void testMethod(String className, String methodName, String params[]) {
        System.out.printf("Test Class %s, Method %s, Params %s.\n", className, methodName, Arrays.toString(params));
        try {
            Class c = Class.forName(className);
            c.getMethod(methodName, getClasses(params));
        } catch (ClassNotFoundException | NoSuchMethodException e) {
            fail();
        }
    }

    /**
     * Test a class with given method parameter and correct modifier
     *
     * @param className
     * @param methodName
     * @param params
     */
    public static void testMethodModifier(String className, String methodName, int modifier, String params[]) {
        System.out.printf("Test Class %s, Method %s, Modifier %d, Params %s.\n", className, methodName, modifier, Arrays.toString(params));
        try {
            Class c = Class.forName(className);
            Method m = c.getMethod(methodName, getClasses(params));

            Assert.assertTrue((m.getModifiers() & modifier) != 0);
        } catch (ClassNotFoundException | NoSuchMethodException e) {
            fail();
        }
    }

    /**
     * Test a class with given method parameter and return type
     *
     * @param className
     * @param methodName
     * @param returnType
     * @param params
     */
    public static void testMethodReturn(String className, String methodName, String returnType, String params[]) {
        System.out.printf("Test Class %s, Method %s, Return %s, Params %s.\n", className, methodName, returnType, Arrays.toString(params));
        try {
            Class c = Class.forName(className);
            Method m = c.getMethod(methodName, getClasses(params));

            assertEquals(returnType, m.getReturnType().toString());
        } catch (ClassNotFoundException | NoSuchMethodException e) {
            fail();
        }
    }

    public static void testMethodsCount(String className, int count) {
        System.out.printf("Test Class %s, Method(s) %d.\n", className, count);
        try {
            Class c = Class.forName(className);
            assertEquals(count, c.getDeclaredMethods().length);
        } catch (ClassNotFoundException e) {
            fail();
        }
    }

    public static void testMethodResult(ReflectionHelper.ClassDescription.Method method, BaseStrictClassTest.WrapConstructorFunction<Object> construction, Object[] params, Object expecetedValue) {
        try {
            System.out.printf("Test Object %s, Method %s, Params %s, Expected %s.\n",
                    construction.apply().getClass().getName(),
                    method,
                    Arrays.toString(params),
                    expecetedValue);
            Object re = ReflectionHelper.invokeFunction(method, construction, params);
            assertEquals(expecetedValue, re);
        } catch (ClassNotFoundException | NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
            fail();
        }
    }
}