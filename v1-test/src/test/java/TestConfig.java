import java.lang.reflect.Modifier;

public class TestConfig {

    public static final String FIGHTER = "Fighter";
    public static final String COMPLEX = "Complex";
    public static final String KNIGHT = "Knight";
    public static final String WARRIOR = "Warrior";
    public static final String PALADIN = "Paladin";
    public static final String DEATH_EATER = "DeathEater";
    public static final String COMBATABLE_GETCOMBATSCORE = "getCombatScore";

    public static final String MONSTER = "Monster";
    public static final String COMBATABLE = "Combatable";

    public static final ReflectionHelper.ClassDescription.Method METHOD__COMBATABLE_GETCOMBATSCORE
            = new ReflectionHelper.ClassDescription.Method(COMBATABLE_GETCOMBATSCORE, Modifier.PUBLIC, double.class.getName(), null);

}