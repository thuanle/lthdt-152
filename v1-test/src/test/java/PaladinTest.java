import org.junit.Test;

import java.util.Arrays;

public class PaladinTest extends BaseCombatableTest {
    public PaladinTest() {
        super(ReflectionHelper.ClassDescriptionBuilder
                .name(TestConfig.PALADIN)
                .baseClass(TestConfig.KNIGHT)
                .addConstructor(int.class, int.class)
                .addMethod(TestConfig.METHOD__COMBATABLE_GETCOMBATSCORE)
                .build());
    }

    @Test
    public void testCombatScore1() {
        int bhp = RandomHelper.nextIntNotIn(0, 900, FIBOS);
        double rhp = bhp * 3;

        testHumanCombatScore(GROUND_NORMAL_1, bhp, WP_0, rhp);
    }

    @Test
    public void testCombatScore2() {
        int bhp = RandomHelper.nextIntNotIn(0, 900, FIBOS);
        double rhp = bhp * 3;

        testHumanCombatScore(GROUND_PRIME, bhp, WP_1, rhp);
    }
    @Test
    public void testCombatScore3() {
        int bhp = RandomHelper.nextIntNotIn(0, 900, FIBOS);
        double rhp = bhp * 3;

        testHumanCombatScore(GROUND_SQUARE, bhp, WP_0, rhp);
    }



    @Test
    public void testCombatScore4() {
        int bhp = RandomHelper.nextIntIn(FIBOS, 3);
        double rhp = 1000 + Arrays.binarySearch(FIBOS, bhp);

        testHumanCombatScore(GROUND_NORMAL_2, bhp, WP_1, rhp);
    }

    @Test
    public void testCombatScore5() {
        int bhp = RandomHelper.nextIntIn(FIBOS, 3);
        double rhp = 1000 + Arrays.binarySearch(FIBOS, bhp);

        testHumanCombatScore(GROUND_PRIME, bhp, WP_0, rhp);
    }

    @Test
    public void testCombatScore6() {
        int bhp = RandomHelper.nextIntIn(FIBOS, 3);
        double rhp = 1000 + Arrays.binarySearch(FIBOS, bhp);

        testHumanCombatScore(GROUND_SQUARE, bhp, WP_1, rhp);
    }
}
