import java.util.Objects;

/**
 * Created by thuanle on 5/12/16.
 */
public abstract class BaseCombatableTest extends BaseStrictClassTest {
    public static final int GROUND_NORMAL_1 = 123;
    public static final int GROUND_NORMAL_2 = 456;
    public static final int GROUND_SQUARE = 25;
    public static final int WP_0 = 0;
    public static final int WP_1 = 1;
    public static final int GROUND_PRIME = 7;
    public static final int[] FIBOS = new int[]{0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987};

    public BaseCombatableTest(ReflectionHelper.ClassDescription description) {
        super(description);
    }

    protected void testMonsterCombatScore(int ground, double re, double im, double rhp) {
        System.out.format("Ground %s, re %s, im %s\n", ground, re, im);
        testFunctionResult(TestConfig.METHOD__COMBATABLE_GETCOMBATSCORE,
                () -> {
                    Battle.GROUND = ground;
                    Object param = ReflectionHelper.createInstance(TestConfig.COMPLEX, new Class[]{double.class, double.class}, new Object[]{re, im});
                    return ReflectionHelper.createInstance(mDescription.getName(), new String[]{TestConfig.COMPLEX},new Object[]{param});
                },
                null,
                rhp);
    }

    protected void testHumanCombatScore(int ground, int bhp, int wp, double rhp) {
        System.out.format("Ground %s, bhp %s, wp %s\n", ground, bhp, wp);
        testFunctionResult(TestConfig.METHOD__COMBATABLE_GETCOMBATSCORE,
                () -> {
                    Battle.GROUND = ground;
                    return ReflectionHelper.createInstance(mDescription.getName(), new Class[]{int.class, int.class}, new Object[]{bhp, wp});
                },
                null,
                rhp);
    }
}
