import org.junit.Test;

/**
 * Created by thuanle on 5/11/16.
 */
public class WarriorTest extends BaseCombatableTest {

    public WarriorTest() {
        super(ReflectionHelper.ClassDescriptionBuilder
                .name(TestConfig.WARRIOR)
                .baseClass(TestConfig.FIGHTER)
                .addConstructor( int.class, int.class)
                .addMethod(TestConfig.METHOD__COMBATABLE_GETCOMBATSCORE)
                .build());
    }

    @Test
    public void testCombatScore1() {
        int bhp = RandomHelper.nextInt(1, 400);
        double rhp = bhp / 10.0;

        testHumanCombatScore(GROUND_NORMAL_1, bhp, WP_0, rhp);
    }


    @Test
    public void testCombatScore2() {
        int bhp = RandomHelper.nextInt(1, 400);
        double rhp = bhp;

        testHumanCombatScore(GROUND_NORMAL_2, bhp, WP_1, rhp);
    }

    @Test
    public void testCombatScore3() {
        int bhp = RandomHelper.nextInt(1, 400);
        double rhp = bhp / 10.0;

        testHumanCombatScore(GROUND_SQUARE, bhp, WP_0, rhp);
    }

    @Test
    public void testCombatScore4() {
        int bhp = RandomHelper.nextInt(1, 400);
        double rhp = bhp;

        testHumanCombatScore(GROUND_SQUARE, bhp, WP_1, rhp);
    }

    @Test
    public void testCombatScore5() {
        int bhp = RandomHelper.nextInt(1, 400);
        double rhp = bhp * 2;

        testHumanCombatScore(GROUND_PRIME, bhp, WP_0, rhp);
    }

    @Test
    public void testCombatScore6() {
        int bhp = RandomHelper.nextInt(1, 400);
        double rhp = bhp * 2;

        testHumanCombatScore(GROUND_PRIME, bhp, WP_1, rhp);
    }


    @Test
    public void testCombatScore11() {
        int bhp = RandomHelper.nextInt(1, 400);
        double rhp = bhp / 10.0;

        testHumanCombatScore(GROUND_NORMAL_1, bhp, WP_0, rhp);
    }


    @Test
    public void testCombatScore22() {
        int bhp = RandomHelper.nextInt(1, 400);
        double rhp = bhp;

        testHumanCombatScore(GROUND_NORMAL_2, bhp, WP_1, rhp);
    }

    @Test
    public void testCombatScore33() {
        int bhp = RandomHelper.nextInt(1, 400);
        double rhp = bhp / 10.0;

        testHumanCombatScore(GROUND_SQUARE, bhp, WP_0, rhp);
    }

    @Test
    public void testCombatScore44() {
        int bhp = RandomHelper.nextInt(1, 400);
        double rhp = bhp;

        testHumanCombatScore(GROUND_SQUARE, bhp, WP_1, rhp);
    }

    @Test
    public void testCombatScore55() {
        int bhp = RandomHelper.nextInt(1, 400);
        double rhp = bhp * 2;

        testHumanCombatScore(GROUND_PRIME, bhp, WP_0, rhp);
    }

    @Test
    public void testCombatScore66() {
        int bhp = RandomHelper.nextInt(1, 400);
        double rhp = bhp * 2;

        testHumanCombatScore(GROUND_PRIME, bhp, WP_1, rhp);
    }
}
