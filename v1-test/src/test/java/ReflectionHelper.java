import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by thuanle on 5/12/16.
 */
public class ReflectionHelper {

    protected static Object createInstance(String className, String[] symbolParams, Object[] realParam) throws ClassNotFoundException, InstantiationException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        Class c = Class.forName(className);
        return c.getConstructor(UtilityTest.getClasses(symbolParams)).newInstance(realParam);
    }

    protected static Object createInstance(String className, Class[] symbolParams, Object[] realParam) throws ClassNotFoundException, InstantiationException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        Class c = Class.forName(className);
        return c.getConstructor(symbolParams).newInstance(realParam);
    }

    public static Object invokeFunction(ClassDescription.Method method,
                                        BaseStrictClassTest.WrapConstructorFunction<Object> construction,
                                        Object[] realParams) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        Object o = construction.apply();
        return o.getClass()
                .getMethod(method.getName(), UtilityTest.getClasses(method.getParams()))
                .invoke(o, realParams);
    }

    public static class ClassDescription {
        private ArrayList<String[]> mConstructors;
        private ArrayList<Method> mMethods;
        private ArrayList<String> mInterfaces;
        private ArrayList<Field> mFields;
        private String mName;
        private String mBase;

        public String getBase() {
            return mBase;
        }

        public ArrayList<String[]> getConstructors() {
            return mConstructors;
        }

        public ArrayList<Field> getFields() {
            return mFields;
        }

        public ArrayList<String> getInterfaces() {
            return mInterfaces;
        }

        public ArrayList<Method> getMethods() {
            return mMethods;
        }

        public String getName() {
            return mName;
        }

        public static class Method {

            private final String mName;
            private final int mModifier;
            private final String[] mParams;
            private final String mReturnType;

            public Method(String name, int modifier, String returnType, String[] params) {
                mName = name;
                mModifier = modifier;
                mReturnType = returnType;
                mParams = params;
            }

            public int getModifier() {
                return mModifier;
            }

            public String getName() {
                return mName;
            }

            public String[] getParams() {
                return mParams;
            }

            public String getReturnType() {
                return mReturnType;
            }

            @Override
            public String toString() {
                return String.format("%s(%s) -> %s",
                        mName,
                        mParams == null ? "" : Arrays.stream(mParams).reduce((p1, p2) -> p1 + " x " + p2).get(),
                        mReturnType);
            }
        }

        public static class Field {
            private final String mName;
            private final int mModifier;
            private final String mType;

            public Field(String name, int modifier, String type) {
                mName = name;
                mModifier = modifier;
                mType = type;
            }

            public int getModifier() {
                return mModifier;
            }

            public String getName() {
                return mName;
            }

            public String getType() {
                return mType;
            }
        }
    }


    public static class ClassDescriptionBuilder {
        private final String mName;
        private final ArrayList<String[]> mConstructors;
        private final ArrayList<ReflectionHelper.ClassDescription.Method> mMethods;
        private final ArrayList<String> mInterfaces;
        private final ArrayList<ReflectionHelper.ClassDescription.Field> mFields;
        private String mBase;

        private ClassDescriptionBuilder(String name) {
            mName = name;
            mConstructors = new ArrayList<>();
            mMethods = new ArrayList<>();
            mInterfaces = new ArrayList<>();
            mFields = new ArrayList<>();
        }

        public static ClassDescriptionBuilder name(String name) {
            return new ClassDescriptionBuilder(name);
        }

        public ClassDescriptionBuilder addConstructor(Class<?>... param) {
            mConstructors.add(Arrays.stream(param)
                    .map(Class::getName)
                    .toArray(String[]::new));
            return this;
        }

        public ClassDescriptionBuilder addConstructor(String... param) {
            mConstructors.add(param);
            return this;
        }

        public ClassDescriptionBuilder addField(String name, int modifier, String type) {
            mFields.add(new ReflectionHelper.ClassDescription.Field(name, modifier, type));
            return this;
        }

        public ClassDescriptionBuilder addInterface(String name) {
            mInterfaces.add(name);
            return this;
        }

        public ClassDescriptionBuilder addMethod(ReflectionHelper.ClassDescription.Method method) {
            mMethods.add(method);
            return this;
        }

        public ClassDescriptionBuilder baseClass(String base) {
            mBase = base;
            return this;
        }


        public ReflectionHelper.ClassDescription build() {
            ReflectionHelper.ClassDescription des = new ReflectionHelper.ClassDescription();
            des.mName = mName;
            des.mBase = mBase;
            des.mConstructors = mConstructors;
            des.mMethods = mMethods;
            des.mInterfaces = mInterfaces;
            des.mFields = mFields;
            return des;
        }
    }

}
