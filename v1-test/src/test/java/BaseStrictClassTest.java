import org.junit.Test;

import java.lang.reflect.InvocationTargetException;

public abstract class BaseStrictClassTest {
    protected final ReflectionHelper.ClassDescription mDescription;

    public BaseStrictClassTest(ReflectionHelper.ClassDescription description) {
        mDescription = description;
    }

    @Test
    public void testBase() {
        String name = mDescription.getName();
        if (name != null && !name.isEmpty()) {
            UtilityTest.testClassBase(name, mDescription.getBase());
        }
    }



    @Test
    public void testConstructorCount() {
        UtilityTest.testConstructorCount(mDescription.getName(), mDescription.getConstructors().size());
    }

//    @Test
//    public void testFields() {
//        mDescription.getFields()
//                .forEach(field -> UtilityTest.testField(mDescription.getName(), field.getName(), field.getModifier(), field.getType()));
//    }

    @Test
    public void testConstructors() {
        mDescription.getConstructors()
                .forEach(con -> UtilityTest.testConstructor(mDescription.getName(), con));
    }

    @Test
    public void testFieldCount() {
        UtilityTest.testFieldCount(mDescription.getName(), mDescription.getFields().size());
    }

    protected void testFunctionResult(ReflectionHelper.ClassDescription.Method method, WrapConstructorFunction<Object> construction, Object[] params, Object expecetedValue) {
        UtilityTest.testMethodResult(method, construction, params, expecetedValue);
    }

    @Test
    public void testInterfaces() {
        mDescription.getInterfaces()
                .forEach(s -> UtilityTest.testClassInterface(mDescription.getName(), s));
    }

    @Test
    public void testInterfaceCount() {
        UtilityTest.testInterfaceCount(mDescription.getName(), mDescription.getInterfaces().size());
    }

    @Test
    public void testMethodCounts() {
        UtilityTest.testMethodsCount(mDescription.getName(), mDescription.getMethods().size());
    }

    @Test
    public void testMethodsModifier() {
        mDescription.getMethods()
                .forEach(method -> UtilityTest.testMethodModifier(mDescription.getName(), method.getName(), method.getModifier(), method.getParams()));
    }

    @Test
    public void testMethodsParams() {
        mDescription.getMethods()
                .forEach(method -> UtilityTest.testMethod(mDescription.getName(), method.getName(), method.getParams()));
    }

    @Test
    public void testMethodsReturnType() {
        mDescription.getMethods()
                .forEach(method -> UtilityTest.testMethodReturn(mDescription.getName(), method.getName(), method.getReturnType(), method.getParams()));
    }

    @Test
    public void testName() {
        UtilityTest.testClassName(mDescription.getName());
    }

    @FunctionalInterface
    public interface WrapConstructorFunction<R> {
        R apply() throws ClassNotFoundException
                , NoSuchMethodException
                , IllegalAccessException
                , InvocationTargetException
                , InstantiationException;
    }
}
