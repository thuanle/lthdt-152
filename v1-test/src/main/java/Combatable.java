/**
 * {@code Combatable} is the one who can fight
 * 
 */
public interface Combatable {
	/**
	 * The combat score which will be calculate the battle result.
	 * @return the score
	 */
	double getCombatScore();
}
