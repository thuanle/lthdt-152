/**
 * This class represents a Monster. A monster only has mana point and does not have health point.
 *
 */
public abstract class Monster {
	private Complex mMana;

	/**
	 * Constructor
	 * @param mana mana point
	 */
	public Monster(Complex mana) {
		mMana = mana;
	}

	/**
	 * Get the mana point
	 * @return point
	 */
	public Complex getMana() {
		return mMana;
	}

	/**
	 * Set the mana point
	 * @param mana point
	 */
	public void setMana(Complex mana) {
		mMana = mana;
	}

	@Override
	public String toString() {
		return String.format("%s{%s}", getClass().getName(), mMana);
	}
}
