/**
 * Class {@code Fighter} represents a human who can combat 
 */
public abstract class Fighter extends Human implements Combatable{
	private int mWp;

	/**
	 * Constructor
	 * @param baseHp Base Health Point of a Fighter
	 * @param wp Weapon of a Fighter
	 */
	public Fighter(int baseHp, int wp) {
		super(baseHp);
		mWp = wp;
	}

	/**
	 * Return the weapon point
	 * @return point
	 */
	public int getWp() {
		return mWp;
	}

	/**
	 * Set the weapon pint 
	 * @param wp point
	 */
	public void setWp(int wp) {
		mWp = wp;
	}

	@Override
	public String toString() {
		return String.format("%s{%d,%d}", getClass().getName(), getBaseHp(), mWp);
	}
}
