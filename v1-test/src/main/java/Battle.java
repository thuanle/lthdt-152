import java.util.Random;

/**
 * The {@code Battle} class represents a battle between two teams. 
 */
public class Battle {
    private static final double RATE_WIN = 0.5;
    private static final int GROUND_BOUND = 999;

    /**
     * The ground of current battle
     */
    public static int GROUND = 1;

    /**
     * Move current ground to new random ground
     */
    public static void moveRandomGround() {
        Random rand = new Random();
        GROUND = rand.nextInt(GROUND_BOUND) + 1;
        System.out.println(" Moving to ground " + GROUND + ".");
    }
    
    private Combatable[] mTeam1;
    private Combatable[] mTeam2;

    /**
     * Constructor with 2 team
     * @param team1 team 1
     * @param team2 team 2
     */
    public Battle(Combatable[] team1, Combatable[] team2) {
        mTeam1 = team1;
        mTeam2 = team2;
    }

    /**
     * Start simulate a combat
     */
    public void combat() {
        double pr = 0;
        for (int i = 0; i < mTeam1.length; i++) {
            double result = duel(mTeam1[i], mTeam2[i]);

            pr += result;

            if (i == 0 && result >= RATE_WIN){
                moveRandomGround();
            }
        }
        pr /= mTeam1.length;

        System.out.println(" Battle result. pR = " + pr);
    }

    private double duel(Combatable cb1, Combatable cb2) {
        double score1 = cb1.getCombatScore();
        double score2 = cb2.getCombatScore();

        double pr = (score1 - score2 + 999) / 2000.0;
        return pr;
    }
}
