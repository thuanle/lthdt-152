
/**
 * This class represents a human, who has health point
 */
public class Human {
	private int mBaseHp;

	/**
	 * Constructor
	 * @param baseHp base health point
	 */
	public Human(int baseHp) {
		mBaseHp = baseHp;
	}

	/**
	 * Return the base health point 
	 * @return point
	 */
	public int getBaseHp() {
		return mBaseHp;
	}

	/**
	 * Set the base health point 
	 * @param baseHp point
	 */
	public void setBaseHp(int baseHp) {
		mBaseHp = baseHp;
	}
	
	@Override
	public String toString() {
		return String.format("%s{%d}", getClass().getName(), getBaseHp());
	}
}
